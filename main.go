package main

import (
	"bufio"
	"fmt"
	"github.com/k0kubun/pp"
	"io"
	"net"
	"os"
)

func main() {
	ListenServer()
}

func ListenServer() {
	listen, err := net.Listen("tcp", ":"+fmt.Sprint(1491))
	if err != nil {
		pp.Println("Api listen failed " + err.Error())
		os.Exit(1)
	}
	defer listen.Close()

	pp.Println("Api listen, port: " + fmt.Sprint(87))

	for {
		conn, err := listen.Accept()
		if err != nil {
			pp.Println(err.Error())
			continue
		}
		go handlerServer(conn)
	}
}

func handlerServer(conn net.Conn) {
	remoteAddr := ""
	if addr, ok := conn.RemoteAddr().(*net.TCPAddr); ok {
		remoteAddr = addr.IP.String()
	} else {
		pp.Println("Remote addr incorrect")
		conn.Close()
		return
	}
	pp.Println("Remote addr :" + remoteAddr)

	AnswerMessage := "RejectMessage"
	defer conn.Close()

	var (
		buf = make([]byte, 1024)
		r   = bufio.NewReader(conn)
		w   = bufio.NewWriter(conn)
	)

	n, err := r.Read(buf)
	data := string(buf[:n])

	switch err {
	case io.EOF:
		pp.Println("Receive data failed io.EOF : " + err.Error())
		return
	case nil:
		pp.Println(data)
		break
	default:
		pp.Println("Receive data failed: " + err.Error())
		return
	}

	w.Write([]byte(AnswerMessage))
	w.Flush()
	pp.Println("Answer message: " + AnswerMessage)
}
